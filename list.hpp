///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// A generic list class.  May be used as a base class for a Doubly Linked List
///
/// @author Baishen Wang <@todo baishen@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   14_Apr_2021 
/////////////////////////////////////////////////////////////////////////////////


#pragma once

#include "node.hpp"

class DoubleLinkedList {

protected:

   Node* head = nullptr;
   Node* tail = nullptr;

private: 

   unsigned int count = 0;

public: 
   //returns true if list is empty, returns false if it's not
   const bool empty() const;
   void push_front( Node* newNode );
   Node* pop_front();
   Node* get_first() const;
   Node* get_next(const Node* currentNode) const;
   inline unsigned int size() const {return count;};

   void push_back( Node* newNode );
   Node* pop_back();
   Node* get_last() const;
   Node* get_prev( const Node* currentNode ) const;
};

