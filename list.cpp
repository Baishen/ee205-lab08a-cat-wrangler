///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// A generic list class.  May be used as a base class for a Doubly Linked List
///
/// @author Baishen Wang <@todo baishen@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   14_Apr_2021 
/////////////////////////////////////////////////////////////////////////////////


#include <iostream>

#include "list.hpp"


const bool DoubleLinkedList::empty() const {
   //std::cout<<"In empty()" << std::endl;
   if (count == 0) {
      return true;
   } else {
      return false;
   }
}

void DoubleLinkedList::push_front( Node* newNode ) {
   //std::cout<<"In pushfront" << std::endl;

   if(newNode == nullptr){
      return;
   }
   if(empty()){
      count ++;
      newNode-> next = nullptr;
      newNode-> prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   else{
   count ++;
   newNode->next = head;
   newNode->prev = nullptr;
   head->prev = newNode;
   head = newNode;

   }
}


